FROM node:lts-alpine

RUN apk update && \
    apk upgrade && \
    apk add bash bash-completion && \
    mkdir /slides && \
    mkdir /dist

COPY . /data

RUN cd /data && \
    npm install

WORKDIR /data
