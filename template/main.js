import Reveal from 'reveal.js';
import Markdown from 'reveal.js/plugin/markdown/markdown.esm';
import RevealHighlight from 'reveal.js/plugin/highlight/highlight.esm';
import RevealMermaid from 'reveal.js-mermaid-plugin/plugin/mermaid/mermaid.esm';
import hljs from 'highlight.js/lib/core';
import 'reveal.js/dist/reveal.css';
import 'reveal.js/dist/theme/white.css';
import 'highlight.js/styles/github.css';
import './main.scss';
import logo from './logo.png'
import meta from '/slides/meta.json';
import slides from '/slides/slides.html';

if (window.location.search.indexOf('print-pdf') > -1) {
  document.getElementById('logo').classList.add('pdf-print');
  document.getElementById('header').classList.add('pdf-print');
  document.getElementById('footer').classList.add('pdf-print');
}

const deck = new Reveal({
  plugins: [
    Markdown,
    RevealHighlight,
    RevealMermaid
  ]
})

deck.initialize({
  slideNumber: 'c/t'
});

hljs.initHighlightingOnLoad();

document.title = meta.title + " | " + meta.author;

const slidesElement = document.getElementById('slides');

const logoDiv = document.createElement('div');
logoDiv.setAttribute('id', 'logo');
const logoImg = document.createElement('img');
logoImg.setAttribute('src', logo);
logoImg.setAttribute('height', 80);
logoImg.setAttribute('border', 0);
logoDiv.appendChild(logoImg);
slidesElement.appendChild(logoDiv);

const header = document.createElement('div');
header.setAttribute('id', 'header');
header.textContent = meta.title;
const footer = document.createElement('div');
footer.setAttribute('id', 'footer');
footer.textContent = meta.author;

slidesElement.appendChild(header);
slidesElement.appendChild(footer);

console.log(slides);

const slidesDoc = new DOMParser().parseFromString(slides, 'text/html');
console.log(slidesDoc);
slidesDoc.body.childNodes.forEach(function(slide) {
  slidesElement.appendChild(slide);
});

console.log(slidesElement);
